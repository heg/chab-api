<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

use Heg\Chab\Models\Logs;
use Heg\Chab\Models\Prestataires;
use Heg\Chab\Models\Tickets;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use \GuzzleHttp\Client as GuzzleClient;
require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

/**
 * The routing middleware should be added earlier than the ErrorMiddleware
 * Otherwise exceptions thrown from it will not be handled by the middleware
 */
$app->addRoutingMiddleware();

$dotenv = Dotenv\Dotenv::createMutable(__DIR__ . '/..');
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS', 'SUB_FOLDER']);
$dotenv->ifPresent('DEBUG')->isBoolean();

$pdo = new PDO('mysql:host=' . $_ENV['DB_HOST'] . ';dbname=' . $_ENV['DB_NAME'],
    $_ENV['DB_USER'], $_ENV['DB_PASS']);

$app->setBasePath($_ENV['SUB_FOLDER']);

$app->add(function (Request $request, RequestHandler $handler) {
    $response = $handler->handle($request);
    return $response
        ->withHeader('Content-Type', 'application/json');
});

$app->get('/', function (Request $request, Response $response, $args) {
    $response->getBody()->write(json_encode([
        'API'    => 'Chab-API',
        'routes' => [
            'public' => [
                'GET /public/tickets/random' => 'random ticket values',
                'GET /public/tickets/{code}' => 'ticket values',
                'GET /public/prestataires'   => 'prestataire list',
                'POST /public/login'         => 'public login',
            ],
            'admin'  => [
                'GET /admin/tickets/{code}'                                   => 'ticket values with admin info',
                'PUT /admin/tickets/{code}/circulation/active|idle|destroyed' => 'Change circulation state',
            ],
        ],
    ]));
    return $response;
});

$app->group('/public', function (RouteCollectorProxy $group) use ($pdo) {

    $group->post('/login', function (Request $request, Response $response) {
        $data     = json_decode(file_get_contents('php://input'), true);
        $login    = filter_var($data['login'], FILTER_SANITIZE_STRING);
        $password = filter_var($data['password'], FILTER_SANITIZE_STRING);

        $token  = base64_encode($login . ':' . $password);
        $client = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => 'https://monnaie-chablais.fr/',
            'headers'  => [
                'Authorization' => ['Basic ' . $token],
            ],
        ]);

        $wpResponse = $client->request('GET', 'wp-json/wp/v2/users/me', ['query' => ['context' => 'edit']]);
        $code       = $wpResponse->getStatusCode();   // 200
        $reason     = $wpResponse->getReasonPhrase(); // OK
        $wpResponse = json_decode((string) $wpResponse->getBody(), true);
        $name       = $wpResponse['name'];
        $roles      = $wpResponse['roles'];

        foreach ($roles as $role) {
            if ($role == 'um_comptoir-de-change') {
                $role = "comptoir";
                break;
            } elseif ($role == 'um_banque-centrale') {
                $role = "bc";
            }
        }

        $response->getBody()->write(json_encode(array(
            'name'  => $name,
            'role'  => $role,
            'token' => $token,
        )));

        return $response;
    });

    $group->get('/prestataires', function ($request, $response, array $args) use ($pdo) {
        $prestataires = new Prestataires($pdo);
        $prestataires = $prestataires->getAll([], 1000);
        $response->getBody()->write(json_encode($prestataires));

        return $response;
    });

    $group->get('/tickets/random', function ($request, $response, array $args) use ($pdo) {
        extract($args);
        $tickets = new Tickets($pdo);
        $ticket  = $tickets->getOne(['id' => rand(1, 1000)]);

        if (!$ticket) {
            throw new \Exception('ticket not found', 404);
        }

        unset($ticket->id);
        $response->getBody()->write(json_encode($ticket));

        return $response;
    });

    $group->get('/tickets/{code}', function ($request, $response, array $args) use ($pdo) {
        extract($args);
        $tickets = new Tickets($pdo);
        $ticket  = $tickets->getOne(['code' => $code]);

        if (!$ticket) {
            throw new \Exception('ticket not found', 404);
        }

        // $logs = new Logs($pdo);
        // $log  = $logs->add([
        //   'label'     => 'check',
        //   'ticket_id' => $ticket->id,
        //   'info'      => json_encode('public'),
        // ]);

        unset($ticket->id);
        $response->getBody()->write(json_encode($ticket));

        return $response;
    });
});

$app->group('/stats', function (RouteCollectorProxy $group) use ($pdo) {
    $group->get('/tickets/changes', function ($request, $response, array $args) use ($pdo) {
        $input = $request->getQueryParams();
        if (!isset($input['start_date'])) {
          throw new \Exception('query need a "start_date"', 428);
        }
        if (!isset($input['end_date'])) {
          throw new \Exception('query need a "end_date"', 428);
        }

        $user = $input['user'] ?? null;
        $prestataire = $input['prestataire'] ?? null;

        $startDate = getDateTimeAfterValidation($input['start_date'], 'Y-m-d');
        $endDate = getDateTimeAfterValidation($input['end_date'], 'Y-m-d');
        if (!$startDate) {
          throw new \Exception('"start_date" should be "Y-m-d"', 428);
        }
        if (!$endDate) {
          throw new \Exception('"end_date" should be "Y-m-d"', 428);
        }

        $logs = new Logs($pdo);
        $stateChanges  = $logs->getStateChanges($startDate, $endDate, $user, $prestataire);

        $users = [];
        $prestas = [];
        foreach($stateChanges as $key => $stateChangeByState) {
            $users = array_merge(explode('%%', $stateChangeByState->users), $users);
            $prestas = array_merge(explode('%%', $stateChangeByState->prestas), $prestas);
            $state  = $logs->getStateChanges($startDate, $endDate, $user, $prestataire, $stateChangeByState->state);
            $stateChanges[$key]->by_state[$stateChangeByState->state] = $state;
        }
        $users = array_values(array_filter(array_unique($users)));
        $prestas = array_values(array_filter(array_unique($prestas)));
        $response->getBody()->write(json_encode([
            'data' => $stateChanges,
            'meta' => [
                'users' => $users,
                'prestataires' => $prestas,
            ]
        ]));
        return $response;
    });
});

function getDateTimeAfterValidation($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    if (!$d) {
        return false;
    }

    return $d;
}

$app->group('/admin', function (RouteCollectorProxy $group) use ($pdo) {
    $group->group('/tickets', function (RouteCollectorProxy $group) use ($pdo) {

        $group->get('/{code}', function ($request, $response, array $args) use ($pdo) {
            extract($args);
            $tickets = new Tickets($pdo);
            $ticket  = $tickets->getOne(['code' => $code]);

            if (!$ticket) {
                throw new \Exception('ticket not found', 404);
            }

            $logs         = new Logs($pdo);
            $logs         = $logs->getAll(['ticket_id' => $ticket->id], 50);
            $ticket->logs = $logs;

            $response->getBody()->write(json_encode($ticket));

            return $response;
        });

        $group->put('/{code}/circulation/{state:active|idle|destroyed}', function ($request, $response, array $args) use ($pdo) {
            extract($args);
            $body     = json_decode(file_get_contents('php://input'), true);
            if (!isset($body['info'])) {
              $info    = json_encode('state change');
            } else {
              $info    = json_encode($body['info']);
            }

            $tickets = new Tickets($pdo);
            $ticket  = $tickets->getOne(['code' => $code]);

            if (!$ticket) {
                throw new \Exception('ticket not found', 404);
            }

            if ($ticket->state != $state) {
                $tickets->set(['id' => $ticket->id, 'code' => $code], ['state' => $state]);

                $ticket->logs = $logs;
                $logs         = new Logs($pdo);

                $action = $state . 'd';
                if ($state == 'active') {
                    $action = 'activated';
                }
                if ($state == 'destroyed') {
                    $action = 'destroyed';
                }


                $log = $logs->add([
                    'label'     => $action,
                    'ticket_id' => $ticket->id,
                    'info'      => $info,
                ]);

                $ticket = $tickets->getOne(['code' => $code]);
            }

            $response->getBody()->write(json_encode($ticket));

            return $response;
        });
    });
});

/**
 * @param bool $displayErrorDetails -> Should be set to false in production
 * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
 * @param bool $logErrorDetails -> Display error details in error log
 * which can be replaced by a callable of your choice.
 * @param \Psr\Log\LoggerInterface $logger -> Optional PSR-3 logger to receive errors
 *
 * Note: This middleware should be added last. It will not handle any exceptions/errors
 * for middleware added after it.
 */
$errorMiddleware = $app->addErrorMiddleware(array_key_exists('DEBUG', $_ENV) ? $_ENV['DEBUG'] : false, true, true);

$app->run();
