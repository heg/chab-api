<?php

namespace Heg\Chab\Models;

use \GuzzleHttp\Client as GuzzleClient;

class Prestataires extends AbstractDirectory
{
    protected $table   = 'prestataires';
    protected $keyList = 'id, label';
    protected $orderBy = 'label ASC';

    /**
     * Récupère toutes les lignes correspondantes aux critères
     * @param  array  $wheres Tableau correspondant aux données demandées
     * @return stdObject|null         la ligne demandée
     */
    public function getAll($wheres = [], $limit = 10, $offset = 0)
    {
        $client = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => 'https://monnaie-chablaisfr.gogocarto.fr',
        ]);

        $response = $client->request('GET', 'api/elements', [
            'query' => [
                'ontology' => 'gogocompact',
            ],
        ]);
        $code     = $response->getStatusCode();   // 200
        $reason   = $response->getReasonPhrase(); // OK
        $response = json_decode((string) $response->getBody(), true);

        $response = array_map(function ($presta) {
            return [
                'id'    => $presta[0],
                'label' => $presta[1][0],
            ];
        }, $response['data']);

        usort($response, function ($prestaA, $prestaB) {
            return ($prestaA['label'] < $prestaB['label']) ? -1 : 1;
        });

        return $response;
    }
}
