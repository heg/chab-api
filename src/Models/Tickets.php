<?php

namespace Heg\Chab\Models;

class Tickets extends AbstractDirectory
{
    protected $table   = 'tickets';
    protected $keyList = 'id, code, value, state, created_at, updated_at';
}
