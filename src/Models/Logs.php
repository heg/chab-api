<?php

namespace Heg\Chab\Models;

class Logs extends AbstractDirectory
{
    protected $table   = 'logs';
    protected $keyList = 'label, info, created_at';

    /**
     * Récupère tous les changements d'état des tickets entre ces deux dates
     * @param  DateTime $startDate
     * @param  DateTime $endDate
     * @return array
     */
    public function getStateChanges($startDate, $endDate, $user = null, $prestataire = null, $state = null)
    {
        $and = '';

        if ($user) {
            $and .= " AND JSON_VALUE(info, '$.user') LIKE '" . $user . "'";
        }

        if ($prestataire) {
            $and .= " AND JSON_VALUE(info, '$.prestataire.label') LIKE '" . $prestataire . "'";
        }

//GROUP_CONCAT
        if ($state) {
            $sql = <<<SQL
SELECT tickets.value AS value, count(logs.ticket_id) AS ticket_count
FROM `logs`
LEFT JOIN tickets ON tickets.id = logs.ticket_id
WHERE logs.created_at >= "{$startDate->format('Y-m-d')} 00:00:00"
AND logs.created_at <= "{$endDate->format('Y-m-d')} 23:59:59"
AND (JSON_VALUE(info, '$') LIKE "state change"
  OR JSON_VALUE(info, '$.label') LIKE "state change")
AND (logs.label = '{$state}' OR JSON_VALUE(info, '$.action') = '{$state}')
{$and}
GROUP BY tickets.value
SQL;
        } else {
           $this->execute("SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
            $sql = <<<SQL

SELECT
CASE
  WHEN  JSON_VALUE(info, '$.action') IS NULL THEN logs.label
  ELSE  JSON_VALUE(info, '$.action')
 END AS state,
  count(logs.ticket_id) AS ticket_count, sum(tickets.value) as sum, GROUP_CONCAT(DISTINCT(JSON_VALUE(info, '$.user')) SEPARATOR "%%") AS users, GROUP_CONCAT(DISTINCT(JSON_VALUE(info, '$.prestataire.label')) SEPARATOR "%%") AS prestas
FROM `logs`
LEFT JOIN tickets ON tickets.id = logs.ticket_id
WHERE logs.created_at >= "{$startDate->format('Y-m-d')} 00:00:00"
AND logs.created_at <= "{$endDate->format('Y-m-d')} 23:59:59"
AND (JSON_VALUE(info, '$') LIKE "state change"
  OR JSON_VALUE(info, '$.label') LIKE "state change")
{$and}
GROUP by JSON_VALUE(info, '$.action'), logs.label
SQL;
        }

        $sth             = $this->execute($sql);
        $getStateChanges = $sth->fetchAll(\PDO::FETCH_CLASS);

        return $getStateChanges ?: null;
    }
}
