<?php

namespace Heg\Chab\Models;

class AbstractDirectory
{
    protected $pdo;
    protected $table;
    protected $keyList = '*';
    protected $orderBy = 'created_at DESC';

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Construit la requête SQL et le databinding
     * @param  array  $wheres Tableau correspondant aux données demandées
     * @return array['sql' => String, 'dataBind' => Array]
     */
    protected function _constructGetValues($wheres)
    {
        $dataBind = [];
        $whereSql = '1 = 1';
        foreach ($wheres as $key => $value) {
            $whereSql = $whereSql . ' and ';
            $whereSql .= "{$key} LIKE :{$key}";
            $dataBind[$key] = $value;
        }

        return [
            'dataBind' => $dataBind,
            'sql'      => "SELECT {$this->keyList} FROM {$this->table} WHERE {$whereSql}",
        ];
    }

    /**
     * Récupère une ligne en fonction de critères
     * @param  array  $wheres Tableau correspondant aux données demandées
     * @return stdObject|null         la ligne demandée
     */
    public function getOne($wheres = [])
    {
        extract($this->_constructGetValues($wheres));
        $sql    = "{$sql} limit 1";
        $sth    = $this->execute($sql, $dataBind);
        $getOne = $sth->fetchObject();

        return $getOne ?: null;
    }

    /**
     * Récupère toutes les lignes correspondantes aux critères
     * @param  array  $wheres Tableau correspondant aux données demandées
     * @return stdObject|null         la ligne demandée
     */
    public function getAll($wheres = [], $limit = 10, $offset = 0)
    {
        extract($this->_constructGetValues($wheres));
        $sql    = "{$sql} ORDER BY {$this->orderBy} limit {$offset}, {$limit} ";
        $sth    = $this->execute($sql, $dataBind);
        $getOne = $sth->fetchAll(\PDO::FETCH_CLASS);

        return $getOne ?: null;
    }

    /**
     * Ajoute une ligne dans la table
     * @param  array  $data Tableau correspondant aux données de l'objet
     */
    public function add($data = [])
    {
        $dataBind   = [];
        $keyListSql = '';
        $valueSql   = '';
        foreach ($data as $key => $value) {
            $valueSql = $valueSql ? $valueSql . ', ' : '';
            $valueSql .= ":{$key}";
            $keyListSql = $keyListSql ? $keyListSql . ', ' : '';
            $keyListSql .= "`{$key}`";
            $dataBind[$key] = $value;
        }
        $sql = "INSERT INTO {$this->table} ({$keyListSql}) VALUES ({$valueSql})";
        $this->execute($sql, $dataBind);

        return $this->getPdo()->lastInsertId();
    }

    /**
     * modifie une ou plusieurs lignes dans la table
     * @param  array  $where Tableau correspondant aux données des objets permettant leur identification
     * @param  array  $data Tableau correspondant aux données à modifier
     */
    public function set($where = [], $data = [])
    {
        $dataBind = [];
        $whereSql = '';
        $valueSql = '';
        foreach ($data as $key => $value) {
            $valueSql = $valueSql ? $valueSql . ', ' : '';
            $valueSql .= "$key = :{$key}";
            $dataBind[$key] = $value;
        }
        foreach ($where as $key => $value) {
            $whereSql = $whereSql ? $whereSql . ' AND ' : '';
            $whereSql .= "$key = :{$key}";
            $dataBind[$key] = $value;
        }
        $sql = "UPDATE {$this->table} SET {$valueSql} WHERE {$whereSql} ";
        $this->execute($sql, $dataBind);
    }

    protected function getPdo()
    {
        return $this->pdo;
    }

    /**
     * Prépare et Execute la requête SQL
     * @param  string $sql    La requête
     * @param  array  $data   Les valeurs de préparations de la requête
     * @return \PDOStatement  La requête exécutée (il ne reste plus qu'à fetcher)
     */
    protected function execute(string $sql, $dataBind = [])
    {
        $pdo    = $this->getPdo();
        $sth    = $pdo->prepare($sql);
        $result = $sth->execute($dataBind);
        if (!$result) {
            $error = $sth->errorInfo();

            if ($error && isset($error[2])) {
                throw new \Exception($error[2], 4001);
            }
        }

        return $sth;
    }
}
