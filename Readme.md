# Chab-Api

## Routes

### GET /public/tickets/{code}

Récupère les données minimales d'un ticket

```json
{
  "code": "PUHFWRSY",
  "value": "1",
  "state": "idle",
  "created_at": "2021-02-24 00:00:00",
  "updated_at": "2021-02-24 13:51:59"
}
```

### GET /public/tickets/random

Récupère les données minimales d'un ticket au hasard

```json
{
  "code": "PUHFWRSY",
  "value": "1",
  "state": "idle",
  "created_at": "2021-02-24 00:00:00",
  "updated_at": "2021-02-24 13:51:59"
}
```

### GET /admin/tickets/{code}

Récupère les données complètes d'un ticket

```json
{
  "id": "2",
  "code": "PUHFWRSY",
  "value": "1",
  "state": "idle",
  "created_at": "2021-02-24 00:00:00",
  "updated_at": "2021-02-24 13:51:59",
  "logs": [
    {
      "label": "check",
      "info": "\"public\"",
      "created_at": "2021-02-24 14:53:25"
    },
    {
      "label": "check",
      "info": "\"admin\"",
      "created_at": "2021-02-24 14:53:19"
    },
    {
      "label": "check",
      "info": "\"admin\"",
      "created_at": "2021-02-24 14:53:18"
    }
  ]
}
```

### PUT /admin/tickets/{code}/circulation/active|idle|destroyed

Change l'état de circulation d'un ticket
Retourne l'état public du ticket

```json
{
  "code": "PUHFWRSY",
  "value": "1",
  "state": "idle",
  "created_at": "2021-02-24 00:00:00",
  "updated_at": "2021-02-24 13:51:59"
}
```

### GET /public/prestataires

Retourne la liste des prestataires

```json
[
  {
    "id": 1,
    "label": "Prestataire 1"
  },
  {
    "id": 2,
    "label": "Prestataire 2"
  }
]
```

## Installation

### Base de données

La structure de la base de données se trouve dans le fichier `/doc/tables.sql`

### Installation des dépendances

via [composer](https://getcomposer.org/)

`$ composer install`

### Modification du fichier .env

Le fichier `.env` a été crée après l'installation (sinon `$ cp .env.example .env`)

Modifiez le valeurs du fichier `.env` pour qu'elle corresponde à votre configuration.

## Apache config

```xml
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```